import { combineReducers } from 'redux';
import initialData from './initialRenderData/dataReducer';
export default combineReducers({
    initialData
});