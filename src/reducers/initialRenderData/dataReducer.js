import { 
    LOGIN_SUCCESS, LOGOUT,
} from '../../types'

const initialState = {
    connected: '', 
}
export default function(state = initialState, action){
    switch (action.type) {
        case LOGIN_SUCCESS:
            localStorage.setItem('connected',true);
            window.location = '/users';
            return {
                ...state,
                connected:true
            }
        case LOGOUT:
            localStorage.clear();
            window.location = '/login';
            return {
                ...state,
                connected:false
            } 
        default:
            return state;
    }
}