import UserAxios from "../config/axiosUser";
import { API_USERS } from "../utils/UrlWebServices";


/**
* obtener los usuarios
*
* @return  {data} lista de usuarios 
*
 */
export function getUsers(){

    return async (dispatch) => {
    
        try {
            const response = await UserAxios.get(API_USERS);

            if( response.data.success !== true){
                return {
                    message: 'Error interno, Favor de intentar mas tarde', 
                    severity: 'error'
                };
            }

            return{
                message: response.data.data.employees,
                severity: 'success'
            } 
        
        } catch (error) {


            return {
                message: 'Error interno, Favor de intentar mas tarde', 
                severity: 'error'
            };
            
        }
    }
}


/**
* obtener los usuarios
*
* @return  {data} lista de usuarios 
*
 */
export function newUser(data){

    return async (dispatch) => {
    
        try {
            const response = await UserAxios.post(API_USERS, data);

            console.log(response)

            if( response.data.success !== true){
                return {
                    message: 'Error interno, Favor de intentar mas tarde', 
                    severity: 'error'
                };
            }

            return{
                message: response.data.data,
                severity: 'success'
            } 
        
        } catch (error) {


            return {
                message: 'Error interno, Favor de intentar mas tarde', 
                severity: 'error'
            };
            
        }
    }
}