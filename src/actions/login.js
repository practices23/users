import { LOGIN_SUCCESS, LOGOUT } from "../types";


/**
 * Action para iniciar session 
 * 
 * @returns 
 */
export function login(  ){

    return (dispatch) => 
        dispatch({ 
            type: LOGIN_SUCCESS,
        });

}


/**
 * Action para cerrar session 
 * 
 * @returns 
 */
export function logout(  ){

    return (dispatch) => 
        dispatch({ 
            type: LOGOUT,
        });

}