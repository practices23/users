import React from 'react';

import Login from './pages/login';
import Page404 from './pages/Page404';
import Users from './pages/users';

import {
    BrowserRouter,
    Routes,
    Route,
    Navigate
} from 'react-router-dom';

// ----------------------------------------------------------------------

export default function Router() {

    return(
        <Routes>
            <Route path="/login" element={<Login/>} />
            {/* <Route path="/404" element={<Page404/>} /> */}
            <Route path="/404" element={<Users/>} />
            <Route path="/" element={<Navigate to="/login" />} />
        </Routes>
    )
}
