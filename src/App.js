import { Provider, useSelector } from 'react-redux'
import { Navigate, Route, Routes } from 'react-router-dom';
import Login from './pages/login';
import Page404 from './pages/Page404';
import Users from './pages/users';
import store from './store';

function App() {
  return (
    <Provider store={store}>
      <RouterLinks/>
        
    </Provider>
  );
}

export default App;


const RouterLinks = ()=> {

  const auxConnected = useSelector(state => state.initialData.connected); 
  const connected =  localStorage.getItem('connected');

  console.log(auxConnected)
  console.log(connected===true)

  return(
    connected
    ? 
      <PrivateRoutes/>
    : 
      <PublicRoutes/>
    
  )
}

const PublicRoutes = () =>{
  return(

    <Routes>
      <Route path="/login" element={<Login/>} />
      <Route path="/404" element={<Page404/>} />
      <Route path="/" element={<Navigate to="/login" />} />
      <Route path="*" element={<Navigate to="/404" />} />
    </Routes>
  )
}

const PrivateRoutes = () =>{
  return(
    <Routes>
      <Route path="/login" element={<Navigate to="/users" />} />
      <Route path="/404" element={<Page404/>} />
      <Route path="/users" element={<Users/>} />
      <Route path="/" element={<Navigate to="/users" />} />
      <Route path="*" element={<Navigate to="/404" />} />
    </Routes>
  )
}