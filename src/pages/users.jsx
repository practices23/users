import { Box, Button, Card, CardContent, CardHeader, Container, Modal, Typography } from '@mui/material';
import React, { useEffect } from 'react'
import { useState } from 'react';
import { useDispatch } from 'react-redux'
import { logout } from '../actions/login';
import { getUsers } from '../actions/users';
import NewUser from './newUser';
import { TableUsers } from './tableUsers';

const Users = () => {

    const dispatch = useDispatch();
    const [users, setUsers] = useState([]);
    const [open, setOpen] = useState(false);

    useEffect(() => {
        getUser()
    }, [])

    const getUser = async () =>{
        const response = await dispatch(getUsers());
        setUsers(response.message)
    }
    
    const handleClose = async () =>{
        setOpen(!open)
    }

    const handleOpen = async () =>{
        setOpen(!open)
    }

    const logoutS = async () =>{
        dispatch(logout())
    }

    return (
        <Container >
            <Button onClick={logoutS}> Cerrar Sesion</Button>
            <Card>
                <CardHeader 
                    title={
                        <Typography >
                            Tabla de usuarios
                        </Typography>
                    }
                    action={
                        <Button
                            onClick={handleOpen}
                            variant='outlined'
                            >Nuevo Usuario</Button>
                    }
                />
                <CardContent>
                    <TableUsers row={users} />
                </CardContent>
            </Card>
            <Modal 
                open={open}
                onClose={handleClose}
            >
                <Box  sx={style}>
                    <Card>
                        <CardHeader title={
                            <Typography >
                                Nuevo usuario
                            </Typography>}
                            action={<Button onClick={handleClose} >X</Button>}
                            />
                        <CardContent>
                            <NewUser />
                        </CardContent>
                    </Card>
                </Box>
            </Modal>
        </Container>
    )
}

export default Users


const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '50%',
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};