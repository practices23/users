import { Button, Stack, TextField } from '@mui/material'
import { useFormik } from 'formik'
import React from 'react'
import { useDispatch } from 'react-redux'
import { newUser } from '../actions/users'

const NewUser = () => {

    const dispacth = useDispatch();

    const validate = ( values ) => {

        let errors= {};

        if(!values.name){errors.name='El Nombre es obligatorio'}
        if(!values.last_name){errors.last_name='El Apellido es obligatorio'}
        if(!values.birthday){errors.birthday='la fecha de nacimiento es obligatoria'}

        return errors
    }

    const formik = useFormik({
        initialValues:{
            name:'',
            last_name:'',
            birthday:''
        },
        validate,
        onSubmit: values=> handleSend(values)
    })


    const handleSend = async (values) => {

        const response = await dispacth(newUser(values));
        console.log(response)
    }   


    return (
        <form onSubmit={formik.handleSubmit}>
            <Stack spacing={2}>
                <TextField 
                    id='name'
                    value={formik.values.name}
                    onChange={formik.handleChange} 
                    onBlur ={formik.handleBlur}
                    helperText={formik.touched.name ?  formik.errors.name : ''}
                    error={formik.touched.name && formik.errors.name} 
                    label='Nombre'
                    />
                <TextField 
                    id='last_name'
                    value={formik.values.last_name}
                    onChange={formik.handleChange} 
                    onBlur ={formik.handleBlur}
                    helperText={formik.touched.last_name ?  formik.errors.last_name : ''}
                    error={formik.touched.last_name && formik.errors.last_name} 
                    label='Apellido'
                    />
                <TextField 
                    id='birthday'
                    type='date'
                    value={formik.values.birthday}
                    onChange={formik.handleChange} 
                    onBlur ={formik.handleBlur}
                    helperText={formik.touched.birthday ?  formik.errors.birthday : ''}
                    error={formik.touched.birthday && formik.errors.birthday} 
                    label='Fecha de Nacimiento'
                    />
                <Button type='submit'> Enviar </Button>
            </Stack>
        </form>
    )
}

export default NewUser