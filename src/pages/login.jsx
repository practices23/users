import { Button, Card, CardContent, CardHeader, Container, Stack, TextField, Typography } from '@mui/material'
import { useFormik } from 'formik';
import React from 'react'
import { useDispatch } from 'react-redux';
import { login } from '../actions/login';

const Login = () => {

    const dispacth=useDispatch();

    const validate = ( values ) => {

        let errors= {};

        if(!values.user){ 
            errors.user='El usuario es obligatorio'
        }
        if(!values.password){ 
            errors.password='la constraseña es obligatoria'
        }
        return errors
    }

    const formik = useFormik({
        initialValues:{
            user:'',
            password:''
        },
        validate,
        onSubmit: values => handleSend(values)
    })
    
    const handleSend = async () => {
        await dispacth(login())
    }


    return (
        <Container >
            <Card>
                <CardHeader title={
                    <Typography >
                        Iniciar Sesion
                    </Typography>
                } />
                <CardContent>
                    <form onSubmit={formik.handleSubmit} >
                        <Stack spacing={2}>
                            <TextField 
                                id='user'
                                value={formik.values.user}
                                onChange={formik.handleChange} 
                                onBlur ={formik.handleBlur}
                                helperText={formik.touched.user ?  formik.errors.user : ''}
                                error={formik.touched.user && formik.errors.user} 
                                label='Usuario'
                                />
                            <TextField 
                                id='password'
                                type='password'
                                value={formik.values.password}
                                onChange={formik.handleChange} 
                                onBlur ={formik.handleBlur}
                                helperText={formik.touched.password ?  formik.errors.password : ''}
                                error={formik.touched.password && formik.errors.password} 
                                label='Usuario'
                                />
                            <Button type='submit'> Iniciar </Button>
                        </Stack>
                    </form>
                </CardContent>
            </Card>
        </Container>
    )
}

export default Login

